#include <iostream>
#include "parser.hpp" // https://github.com/AriaFallah/csv-parser
#include "parsestring.h"

using namespace std;

/*
Parse massive csv file to get rid of entries with multiple attributes
When reading in data, fields are sorted in alphabetical order. which is kind of
annoying. below is a list of fields in order:

backers_count,blurb,category,converted_pledged_amount,country,created_at,creator,
currency,currency_symbol,currency_trailing_code,current_currency,deadline,
disable_communication,friends,fx_rate,goal,id,is_backing is_starrable,
is_starred,launched_at,location name,permissions,photo,pledged,profile,slug,
source_url,spotlight,staff_pick,state,state_changed_at,static_usd_rate,
urls usd_pledged,usd_type,j

Csv file format by row
  1. backers_count
  2. blurb
  3. category (needs to be parsed)
  4. converted_pledged_amount
  5. country
  6. created_at
  7. creator (needs to be parsed)
  8. currency
  9. currency_symbol
  10. currency_trailing_code
  11. current_currency
  12. deadline
  13. disable_communication
  14. friends
  15. fx_rate
  16. goal
  17. id
  18. is_backing
  19. is_starrable
  20. is_starred
  21. launched_at
  22. location (needs to be parsed)
  23. permissions
  24. photo
  25. pledged
  26. profile (needs to be parsed, if field is needed)
  27. slug
  28. source_url
  29. spotlight
  30. staff_pick
  31. state
  32. state_changed_at
  33. static_usd_rate
  34. urls (needs to be parsed if needed)
  35. usd_pledged
  36. usd_type
  37. usd_pledged
  38. usd_type
*/

enum indices
{
  FieldNames = 0,
  Category = 2,
  Location = 21,
  Blurb = 1,
  Creator = 5,
  Photo = 22,
  Profile = 23,
  Url = 30,
  Name = 20
};

int main(int argc, char *argv[])
{
  string filepath = argv[1];
  //cout << filepath << endl;
  ifstream file;
  file.clear();
  file.open(filepath);
  aria::csv::CsvParser parser(file);

  vector<vector<string>> lines;

  //read through each line of the file. use lines, a vector of string vectors,
  //to hold all lines of the file. each vector in lines will contain the fields
  //for each line.
  if(file.is_open())
  {
    if (file.rdstate())
    {
      file.clear();
      file.close();
      return 0;
    }
    for (auto& row : parser) {
      vector<string> currentrow;
      for (auto& field : row) {
        currentrow.push_back(field);
      }
      lines.push_back(currentrow);
    }
    file.clear();
    file.close();
  }

  //filter out fields that are super long or contain multiple values

  for (auto& vect : lines)
  {
    for (int i = 0; i < vect.size(); i++)
    {
      if (i == indices::Category) //filter category
      {
		    string filtered = parseCategory(vect.at(i));
		    vect.at(i) = filtered;
      }
      if (i == indices::Location) //filter location
      {
        string filtered = parseLocation(vect.at(i));
        vect.at(i) = filtered;
      }
    }
  }
  //Remove all unneeded fields, or fields that are just too long
  for (auto& vect : lines)
  {
	  for (int i = 0; i < vect.size(); i++)
	  {
		  if (i == indices::Blurb) //"blurb"
		  {
			  vect.erase(vect.begin() + (i));
		  }
		  if (i == indices::Creator) //creator
		  {
			  vect.erase(vect.begin() + (i));
		  }
		  if (i == indices::Photo) //photo
		  {
			 vect.erase(vect.begin() + (i));
		  }
      if (i == indices::Profile) //profile
      {
        vect.erase(vect.begin() + (i));
      }
      if (i == indices::Url) //url
      {
        vect.erase(vect.begin() + i);
      }
      if (i == indices::Name) //name
      {
        vect.erase(vect.begin() + i);
      }
	  }
  }

  //remove titles that are not needed. If this is not the first file open,
  //we do not need the field names. We also do not need field names
  //of deleted fields.

  if (filepath == "/home/jaret2/Desktop/567/project2/CSCE567-Project-2/data/Kickstarter_2019-03-14/Kickstarter001.csv")
  {
    //parse line with field names - remove titles for deleted fields
    vector<string> firstline = lines.at(0);
    string currentline = "";
    vector<string> modifiedLine;
    for (int i = 0; i < firstline.size(); ++i)
    {
	     string fieldName = firstline.at(i);
	      if (!(fieldName == "photo"
            || fieldName == "profile"
            || fieldName == "source_url"
            || fieldName == "urls"))
	       {
          currentline += fieldName + ",";
		      modifiedLine.push_back(fieldName);
	       }
     }
     lines.at(0) = modifiedLine;
  }
  else
  {
    lines.erase(lines.begin());
  }

  //write output to standard output

  for(int i = 0; i < lines.size(); i++)
  {
    string currentline = "";
    for(int j = 0; j < lines.at(i).size(); j++)
    {
      string pos = lines.at(i).at(j);
      if (pos.length() < 30)
      {
        currentline += pos + ",";
      }
    }
    cout << currentline << endl;
  }

  return 0;
}
