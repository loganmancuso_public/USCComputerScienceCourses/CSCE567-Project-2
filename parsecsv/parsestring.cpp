#include "parsestring.h"

string parseCategory(string field)
{
  /* Sort through item in category field and get relevant information:
     - desiredField -> field we are searching for
     - quote -> field is contained entirely in quotes
     - find the start index of the desired field, and make a crude cut
       with an arbitrary length value
     - Get the position of the first quotation mark in the crude cut
     - If first quote is found, get the position of the second quote
     - cast to ints to get the length, since c++ substr is annoying
     - extract value from between quotations
     - if trailing quotation, remove it

  */
  string desiredField = "name";
  string quote = "\"";
  size_t found = field.find(desiredField);
  if (field.size() > 24) //Arbitrary value
  {
    string crude = field.substr(15,20);
    size_t pos = crude.find(quote, 1);
    if (pos != string::npos)
    {
      size_t pos2 = crude.find(quote, pos + 2);
      int index1 = static_cast<int>(pos);
      int index2 = static_cast<int>(pos2);
      int length = index2 - index1;
      string category = crude.substr(pos + 1, length);
      if (category.back() == '\"')
      {
        category = category.substr(0, category.size()-1);
      }
      return category;
    }
  }
  return "category"; //first row
}

string parseLocation(std::string field)
{
  /* Sort through item in location field and get relevant information:
     - desiredField -> field we are searching for
     - quote -> field is contained entirely in quotes
     - comma -> remove comma between city and state, because the comma is the
       delimiter - will create errors
     - find the start index of the desired field, and make a crude cut
       with an arbitrary length value
     - get the position of the starting and end quotes, and extract the value
       between them. In this case, we add 19 to the found field so we avoid
       the quotes in the first part of the field, where it says "displayable_name"
     - get the individual city name and state name, and add a hyphen between them
     - return the new location name value
  */
  string desiredField = "displayable_name";
  string quote = "\"";
  string comma = ",";
  size_t found = field.find(desiredField);
  if (field.size() > 45) //45 is arbitrary - most fields we need are shorter
  {
    string crude = field.substr(found+18, 40);
    //cout << crude << endl;
    size_t pos = crude.find(quote);
    if (pos != std::string::npos) //quotation found
    {
      size_t pos2 = crude.find(quote, pos+1);
      if (pos2 != std::string::npos)
      {
        int index = static_cast<int>(pos);
        int index2 = static_cast<int>(pos2);
        int length = index2 - index;
        string placename = crude.substr(pos + 1, length - 1); //got full place name
        //cout << placename << endl;
        //parse to remove commas
        size_t commaindex1 = placename.find(comma);
        if (commaindex1 != string::npos)
        {
          string retval= "";
          string city = placename.substr(0, commaindex1); //city name
          string after = placename.substr(commaindex1+2); //state name
          //cout << "After: "<<  after << endl;
          size_t commaindex2 = after.find(comma);
          retval += city + "-" + after;
          if (commaindex2 != string::npos)
          {
            string newafter = after.substr(0, commaindex2);
            string after2 = after.substr(commaindex2+2);
            retval = city + "-" + newafter + "-" + after2;
          }
          return retval;
        }
      }
    }
  }
  return "location"; //first row
}
